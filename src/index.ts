import express from 'express';

const cookieParser = require('cookie-parser');
const logger = require('morgan');
const indexRouter = require('./routes');
const maRouter = require('./routes/metal-archives');
const cors = require('cors')
const index = express();
index.use(cors())
index.listen(3001)
index.use(logger('dev'));
index.use(express.json());
index.use(express.urlencoded({extended: false}));
index.use(cookieParser());
index.use('/', indexRouter);
index.use('/metal-archives', maRouter);

module.exports = index;
