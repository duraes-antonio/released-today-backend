import axios from 'axios'
import {MAResponse} from '../models/metal-achives';
import {FilterAlbums, MAAlbum} from '../models/album';

export const urlMA = 'https://www.metal-archives.com/search/ajax-advanced/searching/albums'

const handleDateMA = (dateLine: string): Date | null => {
	const dateGroups = dateLine.match(/([aA-zZ]+\s\d{1,2}[aA-zZ,\s]+\d{4})/g)
	if (!dateGroups) {
		return null
	}
	const dateOnly = dateLine.match(/(\d{4}-\d{2}-\d{2})/)
	return dateOnly && new Date(Date.parse(dateOnly[dateOnly.length - 1].replace('-', '/')))
}

const handleImage = async (metalAchivesAlbumUrl: string): Promise<string | null> => {
	const page = await axios.get(metalAchivesAlbumUrl)
	const imgUrlGroups = (page.data as string).match(/.*(<img src=.*>)/g)

	if (!imgUrlGroups || imgUrlGroups.length < 2) {
		return null
	}
	const urlGroups = imgUrlGroups[imgUrlGroups.length - 1].match(/src="([^"]+)"/)
	return urlGroups && urlGroups[urlGroups.length - 1]
}

const handleMAResponse = (res: MAResponse): MAAlbum[] => {
	return res.aaData
		.map(data => {
			const artistGroups = data[0].match(/>(.*)<\//)!
			const nameGroups = data[1].match(/>(.*)<\//)!
			const urlGroups = data[1].match(/=\\?"(.+)">/)!
			const idGroups = data[1].match(/(\d+)+\\?"/)!
			const album: MAAlbum = {
				artist: artistGroups[artistGroups.length - 1],
				date: handleDateMA(data[3]),
				format: data[2],
				id: +idGroups[idGroups.length - 1].split('/'),
				name: nameGroups[nameGroups.length - 1],
				urlAlbum: urlGroups[urlGroups.length - 1]
			}
			return album
		})
}

export const getAlbums = async (f: FilterAlbums): Promise<MAAlbum[]> => {
	const buildUrl = (page = 1) => {
		const releaseTypes = [1, 2, 4, 5, 10, 13]
		const releaseFormats = ['CD', 'Digital']
		const dataProps = [0, 1, 2, 3, 4, 5]
		const queryParams: string[] = [
			`releaseYearFrom=${f.startDate.getFullYear()}`,
			`releaseMonthFrom=${f.startDate.getMonth() + 1}`,
			`releaseYearTo=${f.endDate.getFullYear()}`,
			`releaseMonthTo=${f.endDate.getMonth() + 1}`,
			releaseTypes.map(type => `releaseType[]=${type}`).join('&'),
			releaseFormats.map(format => `releaseFormat[]=${format}`).join('&'),
			dataProps.map(prop => `mDataProp_${prop}=${prop}`).join('&'),
			'sEcho=1',
			'iColumns=6',
			`iDisplayStart=${(page - 1) * 200}`,
			'iDisplayLength=1000'
		]
		return `${urlMA}?${queryParams.join('&')}`
	}
	const req = async (page = 1): Promise<MAAlbum[]> => {
		const maRes = await axios.get(buildUrl(page))
		const albumsAsync = handleMAResponse(maRes.data)
		return await Promise.all(albumsAsync)
	}
	const filterByDate = (albums: MAAlbum[], date: Date): MAAlbum[] => {
		return albums
			.filter(a => !!a.date && a.date.toDateString() === date.toDateString())
	}
	const removeDuplicates = (albums: MAAlbum[]): MAAlbum[] => {
		const artistAlbum = new Map<string, MAAlbum>()
		albums.forEach(a => {
			if (!artistAlbum.has(`${a.artist};${a.name}`)) {
				artistAlbum.set(`${a.artist};${a.name}`, a)
			}
		})
		return Array.from(artistAlbum.values())
	}

	let albums: MAAlbum[] = []
	let page = 1
	let end = false

	while (!end) {
		const lastAlbums = await req(page)
		end = lastAlbums.length < 1 || lastAlbums.some(a => a.date && a.date.getDate() > f.startDate.getDate())
		const filteredAlbums = filterByDate(lastAlbums, f.startDate)
		const albumsPromise = filteredAlbums
			.map(async (a): Promise<MAAlbum> => ({
				...a,
				imageUrl: (await handleImage(a.urlAlbum) ?? undefined)
			}))
		albums = [...albums, ...await Promise.all(albumsPromise)]
		page += 1
	}
	return removeDuplicates(albums)
}

export const metalArchiveService = {
	getAlbums
}
