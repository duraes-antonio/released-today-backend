import {NextFunction, Request, Response} from 'express';

const express = require('express');
const router = express.Router();

router.get(
	'/',
	(req: Request, res: Response, next: NextFunction) =>
    res.status(200).send({'ok': 'Show'})
);

module.exports = router;
