import {NextFunction, Request, Response} from 'express';
import {metalArchiveService} from '../services/metal-archives.service';
import {FilterAlbums} from '../models/album';

const express = require('express');
const router = express.Router();

router.get(
	'/',
	async (req: Request, res: Response, next: NextFunction) => {
		const strIsoToDate = (isoDate?: string): Date | null => {
			return !!isoDate ? new Date(Date.parse(isoDate.replace('-', '/'))) : null
		}
		const startDate: Date = strIsoToDate(req.query.startDate?.toString()) ?? new Date()
		const endDate: Date = strIsoToDate(req.query.endDate?.toString()) ?? startDate
		const filter: FilterAlbums = {startDate, endDate}
		const albums = await metalArchiveService.getAlbums(filter)
		res.status(200).send(albums)
	}
);

module.exports = router
