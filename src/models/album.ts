export interface FilterAlbums {
	startDate: Date
	endDate: Date
}


export interface MAAlbum {
	artist: string
	date: Date | null
	id: number
	imageUrl?: string
	format: string
	name: string
	urlAlbum: string
}

