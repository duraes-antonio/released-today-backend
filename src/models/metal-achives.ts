export interface MAResponse {
	error: string
	iTotalRecords: number
	iTotalDisplayRecords: number
	sEcho: number
	aaData: string[][]
}
